package org.iniad.se.sample13;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MainActivityTest extends TestCase {
    private MainActivity main;
    private float a = 1;
    private float b = 2;
    @Before
    public void setUp() throws Exception {
        main = new MainActivity();
    }
    @After
    public void tearDown() throws Exception {
    }
    @Test
    public void testMakeResult() {
        assertEquals(String.format("%s + %s = %s", a, b, a+b), main.makeResult(Float.toString(a), Float.toString(b), R.id.radioButtonAddition));
        assertEquals(String.format("%s - %s = %s", a, b, a-b), main.makeResult(Float.toString(a), Float.toString(b), R.id.radioButtonSubtraction));
        assertEquals(String.format("%s * %s = %s", a, b, a*b), main.makeResult(Float.toString(a), Float.toString(b), R.id.radioButtonMultiplication));
        assertEquals(String.format("%s / %s = %s", a, b, a/b), main.makeResult(Float.toString(a), Float.toString(b), R.id.radioButtonDivision));
    }
}